# Projet Swing

Le projet ```TrouveTaFormation``` a pour but de mettre à disposition une liste de formations et d'offres d'emplois sur une plateforme web responsive. 
```TrouveTaFormation``` est réalisé dans le cadre d'un projet informatique à l'[IUT Informatique d'Orléans](https://www.univ-orleans.fr/fr/iut-orleans) et met en application le framework Svelte.

## Contributeurs

- [DEVOS Nicolas](https://gitlab.com/nicotoine)
- [TALLAND Kevin](https://gitlab.com/kevintld)

## Installation

### > Pré-requis

[NodeJS](https://nodejs.org/en/) est requis pour lancer ce projet. 

### Commandes 

1. Dans un premier temps, placez-vous dans votre dossier favori prêt à accueillir l'application et récupérez le projet [TrouveTaFormation](https://gitlab.com/kevintld/apprentissage) via la commande :
```bash
git clone https://gitlab.com/kevintld/apprentissage
```

2. Ensuite, installez les dépendances du projet :
```bash
cd apprentissage
npm i
npm run build
```

3. Lancez alors l'application :
```bash
npm run start
```

Le site [TrouveTaFormation](http://localhost:8080) est désormais accessible !

## Informations

Ce projet se base sur les API du Gouvernement Français [La Bonne Alternance](https://api.gouv.fr/documentation/api-la-bonne-alternance) et [Découpage Administratif](https://api.gouv.fr/documentation/api-geo).