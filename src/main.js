import App from './App.svelte';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/style.css';
import './css/map.css';

const app = new App({
	target: document.body,
	props: {}
});

export default app;